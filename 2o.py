#!usr/bin/python

from openpyxl import load_workbook
from openpyxl.utils import get_column_letter
import toml

from utils import *


config = "2o.toml"

print('initializing')
# try to load the config toml and if it doesn't create a default one
try:
    options = toml.load(config)
    print('loaded 2o.toml')
except:
    print("2o.toml was not found. 2o.toml has been created")
    options = {
            'low_ranks': {'filename': 'str.xlsx', 'sheet_name': 'Sheet1', 'names_column': 'A', 'names_row': '6', 'dates_row': '5', 'total_str': '13'},
            'high_ranks': {'filename': 'stelexh.xlsx', 'names_row': '', 'dates_row': '', 'eas': '', 'aydm': '', 'baydm': '', 'skopoi': '', 'pylh': ''},
            'templates': {'daily': 'protypo.xlsx', 'duty_sheet': 'Sheet1', 'date_cell': 'E3', 'bottom_right': 'I20'}}
    with open(config, "w") as f:
        toml.dump(options, f)

# print(options)

# load the low_ranks workbook and set dates_row and names_column
low_duties = load_workbook(filename=options['low_ranks']['filename'])
low_duties = low_duties[options['low_ranks']['sheet_name']]
date_row = options['low_ranks']['dates_row']
names_col = options['low_ranks']['names_column']
names_row = options['low_ranks']['names_row']
total_str = int(options['low_ranks']['total_str'])
print(total_str)


# load the template
template_wb = load_workbook(filename=options['templates']['daily'])
template_sh = template_wb[options['templates']['duty_sheet']]
edge = options['templates']['bottom_right']

# grab the date in the template
current_date = options['templates']['date_cell']
current_date = template_sh[current_date].value

print(current_date)

# find the column that matches the date in the template
col = 1
while True:
    col_letter = get_column_letter(col)
    if low_duties[col_letter + date_row].value == current_date:
        col = col_letter
        print(f'found matching date column in the low_ranks file column: {col}')
        break
    else:
        col += 1


# now col refers to the column in the str.xlsx file that matches the date in the template
# set current row cur_row to date_row+1
cur_row = names_row
for row in range(int(names_row), int(names_row)+total_str+1):
    # print('enter loop')
    name = low_duties[names_col+str(row)].value
    lookup_value = low_duties[col+str(row)].value
    search_and_replace(template_sh, edge, lookup_value, name)

# TODO grab values from stelexh.xlsx file

template_wb.save('test.xlsx')
