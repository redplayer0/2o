from openpyxl.utils import column_index_from_string
from openpyxl.utils import get_column_letter
from re import findall


# little helper function to increment integers if they are strings
def increment_string_integer(x, i):
    return str(int(x)+i)


def search_and_replace(sheet, edge, old, new):
    if old == '':
        return
    max_col, max_row = get_coordinates(edge)
    for row in range(1, max_row+1):
        flag = False
        for col in range(1, max_col+1):
            str_col = get_column_letter(col)
            if sheet[str_col+str(row)].value == old:
                sheet[str_col+str(row)].value = new
                flag = True
                break
        if flag:
            break


def get_coordinates(cell):
    row = findall('[0-9]+', cell)
    col = findall('[a-zA-Z]+', cell)
    return column_index_from_string(col[0]), int(row[0])


# testing
# print(get_coordinates('A200'))
